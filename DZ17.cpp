#include <iostream>
#include <string>
#include <cmath>

using namespace std;

class Developer
{
private:
    int ageIvan = 33;
    char const*firstName()
    {
        return "Ivan";
    }
public:
    int GetAge()
    {
        return ageIvan;
    }
    char const*GetName()
    {
        return firstName();
    }
};


class Vector
{
public:
    Vector() : x(5), y(5), z(5)
    {}
    Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
    {}
    
    double ModuleV() {
        double dVectorLength(0.0);
        dVectorLength = sqrt(x * x + y * y + z * z);
        return dVectorLength;
    }
    void Show()
    {
        std::cout << "\n" << "Vector: " << x << ' ' << y << ' ' << z << "\n" << "ModuleVector: " <<  ModuleV() << "\n";
    }
private:
    double x;
    double y;
    double z;
};


int main()
{
    Developer temp, temp2;
    std::cout << "Name: " << temp.GetName() << "\n" << "Age: " << temp2.GetAge() << "\n";

    Vector v;
    v.Show();
}